﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialButton : MonoBehaviour
{
    //Triggers an event when this button is clicked
    public void ButtonPressed()
    {
        EventBus.TriggerEvent(this, new SequentialTutorialEvents.ButtonPressed());
    }
}
