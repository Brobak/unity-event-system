﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SequentialTutorialManager : MonoBehaviour
{
    public Text textField;

    void Start()
    {
        SetupFirstStep();
    }

    private void SetupFirstStep()
    {
        textField.text = "Press the Return Key";
        //Trigger when a key press event is sent, and the key is Return
        EventBus.AddSingleFireListener<SequentialTutorialEvents.InputKeyPressed>
            (FirstStepCompleted, predicate: e => e.keyPressed == KeyCode.Return);
    }

    private void FirstStepCompleted(object sender, SequentialTutorialEvents.InputKeyPressed e)
    {
        textField.text = "Well done!\nNow just wait a bit...";
        StartCoroutine(WaitAfterFirstStepCoroutine());
    }

    private IEnumerator WaitAfterFirstStepCoroutine()
    {
        yield return new WaitForSeconds(4);
        SetupSecondStep();
    }

    private void SetupSecondStep()
    {
        textField.text = "Now click the button";
        //Trigger when the button is clicked
        EventBus.AddSingleFireListener<SequentialTutorialEvents.ButtonPressed>(SecondStepCompleted);
    }

    private void SecondStepCompleted(object sender, SequentialTutorialEvents.ButtonPressed e)
    {
        textField.text = "Yay! Now just press Space";
        //Trigger when a key press event is sent, and the key is Space
        EventBus.AddSingleFireListener<SequentialTutorialEvents.InputKeyPressed>
            (ThirdStepCompleted, predicate: arg => arg.keyPressed == KeyCode.Space);
    }

    private void ThirdStepCompleted(object sender, SequentialTutorialEvents.InputKeyPressed e)
    {
        textField.text = "Congratulations! You finished the tutorial";
    }
}
