﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    //Fires an event whenever one of a number of keys is pressed
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            EventBus.TriggerEvent(this, new SequentialTutorialEvents.InputKeyPressed(KeyCode.Return));
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            EventBus.TriggerEvent(this, new SequentialTutorialEvents.InputKeyPressed(KeyCode.Space));
        }
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            EventBus.TriggerEvent(this, new SequentialTutorialEvents.InputKeyPressed(KeyCode.LeftControl));
        }
    }
}
