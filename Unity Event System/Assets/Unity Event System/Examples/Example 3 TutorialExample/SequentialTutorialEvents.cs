﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//These are the events to be used for single-fire events in the tutorial example
public abstract class SequentialTutorialEvents : BaseEvent
{
    public class InputKeyPressed : SequentialTutorialEvents
    {
        public KeyCode keyPressed { get; private set; }

        public InputKeyPressed(KeyCode keyPressed)
        {
            this.keyPressed = keyPressed;
        }
    }
    
    public class ButtonPressed : SequentialTutorialEvents
    {
        
    }
}
