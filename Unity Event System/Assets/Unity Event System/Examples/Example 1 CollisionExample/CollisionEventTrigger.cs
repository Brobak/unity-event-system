﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This class fires a collision event whenever the object collides
public class CollisionEventTrigger : MonoBehaviour
{
	void OnCollisionEnter(Collision collision)
	{
		//To trigger an event you need to send the sender (should always be this) and an instance of the event type
		//In this case the event type is the ObjectsCollideEvent, and it needs three additional pieces of information, 
		//which are defined in the event class
		EventBus.TriggerEvent(this, new ExampleEvents.ObjectsCollideEvent(this, collision.collider, collision.contacts[0].point));
	}
}
