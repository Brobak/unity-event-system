﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//This class simply subscribes to an event on awake, however any class can subscribe, at any time
public class CollisionEventListener : MonoBehaviour
{
	public Text collisionText;

	private void Awake()
	{
		//This subscribes to the ObjectsCollideEvent so that onSphereCollision is called when that event is triggered
		//The method needs to take two parameters. The first an object and the second the corresponding event
		EventBus.AddListener<ExampleEvents.ObjectsCollideEvent>(onSphereCollision);
	}

	//sender is the object that triggeres the event. This should almost never be used.	
	//e is the event parameters holding the information sent from the place the event was triggered.
	private void onSphereCollision(object sender, ExampleEvents.ObjectsCollideEvent e)
	{
		//When the event is triggered we get the information sent from the event fire location
		//Here we check that the event was triggered from a Sphere
		if(e.collisionTrigger.gameObject.name == "Sphere")
		{
			collisionText.gameObject.SetActive(true);
		}
	}
}
