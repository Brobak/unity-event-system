﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This class holds the event classes used in this example. All event classes must be subclasses of BaseEvent
//Here ObjectsCollideEvent is further a subclass of ExampleEvents and is also an internal class
//That's not necessary. Making the class internal can help keep your code clean.
//Further subclassing is highly recommended as you can subscribe to a superclass and be notified when any subclass event is fired
public abstract class ExampleEvents : BaseEvent {

	//This is the actual event we will be triggering. The event can be empty
	public class ObjectsCollideEvent : ExampleEvents
	{
		//There doesn't need to be any fields like this. They can be used to send data to the subscribers
		public MonoBehaviour collisionTrigger { get; private set; }

		public Collider collidedWith { get; private set; }

		public Vector3 position { get; private set; }

		//The constructor should assign values to all the properties
		public ObjectsCollideEvent(MonoBehaviour collisionTrigger, Collider collidedWith, Vector3 position)
		{
			this.collisionTrigger = collisionTrigger;
			this.collidedWith = collidedWith;
			this.position = position;
		}
	}

	//This is an abstract event. We can subscribe to this, and be notified whenever any subclass event is triggered
	public abstract class ButtonPressEvent : ExampleEvents
	{
		
	}

	//This is another concrete event, but this is a subclass of the ButtonPressEvent.
	public class ButtonOnePressedEvent : ButtonPressEvent
	{
		
	}

	//We create an event type for each button so we can be notified when a specific button is pressed as well
	public class ButtonTwoPressedEvent : ButtonPressEvent
	{

	}

	//These events are empty meaning they hold no extra information, they will only tell us that the event has been fired
	public class ButtonThreePressedEvent : ButtonPressEvent
	{

	}
}
