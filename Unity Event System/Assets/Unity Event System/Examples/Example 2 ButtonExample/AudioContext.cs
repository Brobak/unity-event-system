﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//A simple class that can play a sound
[RequireComponent(typeof(AudioSource))]
public class AudioContext : MonoBehaviour
{
	private AudioSource audioSource;

	//Here we subscribe to the superclass ButtonPressEvent, which means any subclass of it will trigger this
	void Awake()
	{
		audioSource = GetComponent<AudioSource>();
		EventBus.AddListener<ExampleEvents.ButtonPressEvent>(PlayButtonClick);
	}

	private void PlayButtonClick(object sender, ExampleEvents.ButtonPressEvent e)
	{
		Play();
	}

	//Plays the sound assigned to the ausiosource
	public void Play()
	{
		audioSource.Stop();
		audioSource.Play();
	}
}
