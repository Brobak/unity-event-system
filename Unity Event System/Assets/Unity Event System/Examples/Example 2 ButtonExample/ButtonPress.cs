﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Triggers an event when a button is pressed
public class ButtonPress : MonoBehaviour
{
	[Range(1, 3)]
	public int buttonNumber = 1;

	//This is set to be called in Unity
	public void ButtonPressed()
	{
		switch(buttonNumber)
		{
			case 1:
				EventBus.TriggerEvent(this, new ExampleEvents.ButtonOnePressedEvent());
				break;
			case 2:
				EventBus.TriggerEvent(this, new ExampleEvents.ButtonTwoPressedEvent());
				break;
			case 3:
				EventBus.TriggerEvent(this, new ExampleEvents.ButtonThreePressedEvent());
				break;
			default:
				Debug.LogError("Did not recognize button number. Should be 1 to 3");
				break;
		}
	}
}
