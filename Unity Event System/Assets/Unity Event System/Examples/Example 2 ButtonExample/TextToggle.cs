﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//This class listens for the button press event, and toggles whether the text is shown
public class TextToggle : MonoBehaviour
{
	public Text firstText;
	public Text secondText;
	public Text thirdText;

	//Be aware that if the object starts out deactivated, awake is never called and listeners aren's subscribed
	//However once the the listeners have subscribed, they will be called regardless of whether the gameobject is active or not
	void Awake()
	{
		EventBus.AddListener<ExampleEvents.ButtonOnePressedEvent>(ToggleFirst);
		EventBus.AddListener<ExampleEvents.ButtonTwoPressedEvent>(ToggleSecond);
		EventBus.AddListener<ExampleEvents.ButtonThreePressedEvent>(ToggleThird);
	}

	private void ToggleFirst(object sender, ExampleEvents.ButtonOnePressedEvent e)
	{
		firstText.gameObject.SetActive(!firstText.IsActive());
	}

	private void ToggleSecond(object sender, ExampleEvents.ButtonTwoPressedEvent e)
	{
		secondText.gameObject.SetActive(!secondText.IsActive());
	}

	private void ToggleThird(object sender, ExampleEvents.ButtonThreePressedEvent e)
	{
		thirdText.gameObject.SetActive(!thirdText.IsActive());
	}
}
