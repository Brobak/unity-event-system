﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class SingleFireEventBusEvent<T> : EventBusEvent<T> where T : BaseEvent
{
	public SingleFireEventBusEvent(Action<object, T> action, Predicate<T> predicate) : base(action, predicate) { }

	protected override void Trigger(object sender, T e)
	{
		base.Trigger(sender, e);

        if (predicate.Invoke(e))
        {
		    EventBus.RemoveSingleFireListener(action);
        }
	}
}
