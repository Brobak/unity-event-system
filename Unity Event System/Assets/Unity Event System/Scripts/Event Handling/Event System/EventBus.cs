using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class EventBus
{
    private static readonly Dictionary<Type, List<EventBusEvent>> eventListeners =
        new Dictionary<Type, List<EventBusEvent>>();
    private static readonly Dictionary<Type, List<EventBusEvent>> scenePersistentEventListeners =
        new Dictionary<Type, List<EventBusEvent>>();
    private static readonly Dictionary<IObservable, Dictionary<Type, List<EventBusEvent>>> observableEventListeners =
        new Dictionary<IObservable, Dictionary<Type, List<EventBusEvent>>>();
    private static bool initialized = false;

    ///  <summary>
    ///  Start listening for an event of type T or one of it's subclasses. 
    ///  </summary>
    ///  <param name="scenePersistent">If true listener won't automatically unsubscribe when changing scene
    /// 								  Don't use this unless the object subscribing is also scene persistent</param>
    ///  <param name="predicate">A condition to add to the listener so it only triggers when the condition is true</param>
    public static void AddListener<T>(Action<object, T> listener, bool scenePersistent = false, Predicate<T> predicate = null) where T : BaseEvent
    {
        Dictionary<Type, List<EventBusEvent>> listeners =
            scenePersistent ? scenePersistentEventListeners : eventListeners;

        AddListener(new MultiFireEventBusEvent<T>(listener, predicate), listeners);
    }

    /// <summary>
    /// Removes a specific non-singlefire listener for an event
    /// </summary>
    /// <param name="scenePersistent">Specifies whether the listener to remove is scene persistent</param>
    public static void RemoveListener<T>(Action<object, T> listenerToRemove, bool scenePersistent = false) where T : BaseEvent
    {
        Dictionary<Type, List<EventBusEvent>> listeners =
            scenePersistent ? scenePersistentEventListeners : eventListeners;

        RemoveListener<T, MultiFireEventBusEvent<T>>(listenerToRemove, listeners);
    }

    /// <summary>
    /// Removes all listeners targeting the given subscriber object
    /// </summary>
    public static void RemoveObjectListeners(object listenersObject)
    {
        foreach (var eventListener in eventListeners
            .Concat(scenePersistentEventListeners)
            .Concat(observableEventListeners.SelectMany(kvp => kvp.Value))
            .Select(kvp => kvp.Value))
        {
            int i = 0;
            while (i < eventListener.Count)
            {
                EventBusEvent eventBusEvent = eventListener[i];
                if (eventBusEvent.IsTarget(listenersObject))
                {
                    eventListener.RemoveAt(i);
                    continue;
                }
                i++;
            }
        }
    }

    /// <summary>
    /// Trigger all events listening for type T or one of it's super classes
    /// </summary>
    public static void TriggerEvent<T>(object sender, T eventArgs) where T : BaseEvent
    {
        var listenersToInvoke =
            eventListeners.Concat(scenePersistentEventListeners)
                .Where(kvp => eventArgs.GetType() == kvp.Key || eventArgs.GetType().IsSubclassOf(kvp.Key));
        if (sender is IObservable && observableEventListeners.ContainsKey((IObservable)sender))
        {
            listenersToInvoke = listenersToInvoke.Concat(observableEventListeners[(IObservable)sender])
                .Where(kvp => eventArgs.GetType() == kvp.Key || eventArgs.GetType().IsSubclassOf(kvp.Key));
        }

        if(listenersToInvoke.Any())
        {
            foreach(EventBusEvent eventBusEvent in new List<EventBusEvent>(listenersToInvoke.SelectMany(kvp => kvp.Value)))
            {
                eventBusEvent.Trigger(sender, eventArgs);
            }
        }
    }

    /// <summary>
    /// If you think you need to use this you're probably wrong
    /// </summary>
    public static void RemoveAllListeners()
    {
        Debug.LogWarning("Removing all listeners");
        eventListeners.Clear();
        scenePersistentEventListeners.Clear();
        observableEventListeners.Clear();
    }

    /// <summary>
    /// Start listening for an event of type T and remove the listener the first time it's triggered
    /// </summary>
    ///  <param name="scenePersistent">If true listener won't automatically unsubscribe when changing scene
    /// 								  Don't use this unless the object subscribing is also scene persistent</param>
    ///  <param name="predicate">A condition to add to the listener so it only fires (and removes itself) when the condition is true</param>
    public static void AddSingleFireListener<T>(Action<object, T> listener, bool scenePersistent = false, Predicate<T> predicate = null) where T : BaseEvent
    {
        Dictionary<Type, List<EventBusEvent>> listeners =
            scenePersistent ? scenePersistentEventListeners : eventListeners;

        AddListener(new SingleFireEventBusEvent<T>(listener, predicate), listeners);
    }

    public static void RemoveSingleFireListener<T>(Action<object, T> listenerToRemove, bool scenePersistent = false) where T : BaseEvent
    {
        Dictionary<Type, List<EventBusEvent>> listeners =
            scenePersistent ? scenePersistentEventListeners : eventListeners;

        RemoveListener<T, SingleFireEventBusEvent<T>>(listenerToRemove, listeners);
    }

    #region Observable
    /// <summary>
    /// Start observing this observable for events of type T or one of its subclasses
    /// </summary>
    /// <typeparam name="T">The type of event to listen for</typeparam>
    /// <param name="predicate">A condition to add to the listener so it only triggers when the condition is true</param>
    public static void AddListener<T>(this IObservable observable, Action<object, T> listener, Predicate<T> predicate = null) where T : BaseEvent
    {
        if (!observableEventListeners.ContainsKey(observable))
        {
            observableEventListeners.Add(observable, new Dictionary<Type, List<EventBusEvent>>());
        }
        Dictionary<Type, List<EventBusEvent>> listeners =
            observableEventListeners[observable];

        AddListener(new MultiFireEventBusEvent<T>(listener, predicate), listeners);
    }

    /// <summary>
    /// Removes a specific non-singlefire listener for an event on this observable
    /// </summary>
    public static void RemoveListener<T>(this IObservable observable, Action<object, T> listenerToRemove) where T : BaseEvent
    {
        if (!observableEventListeners.ContainsKey(observable))
        {
            observableEventListeners.Add(observable, new Dictionary<Type, List<EventBusEvent>>());
        }
        Dictionary<Type, List<EventBusEvent>> listeners =
            observableEventListeners[observable];

        RemoveListener<T, MultiFireEventBusEvent<T>>(listenerToRemove, listeners);
    }

    /// <summary>
    /// Start observing this observable for the first event of type T or one of its subclasses
    /// </summary>
    /// <typeparam name="T">The type of event to listen for</typeparam>
    /// <param name="predicate">A condition to add to the listener so it only triggers (and is removed) when the condition is true</param>
    public static void AddSingleFireListener<T>(this IObservable observable, Action<object, T> listener, Predicate<T> predicate = null) where T : BaseEvent
    {
        Dictionary<Type, List<EventBusEvent>> listeners =
            observableEventListeners[observable];

        AddListener(new SingleFireEventBusEvent<T>(listener, predicate), listeners);
    }

    /// <summary>
    /// Removes a specific singlefire listener for an event on this observable
    /// </summary>
    public static void RemoveSingleFireListener<T>(this IObservable observable, Action<object, T> listenerToRemove) where T : BaseEvent
    {
        Dictionary<Type, List<EventBusEvent>> listeners =
            observableEventListeners[observable];

        RemoveListener<T, SingleFireEventBusEvent<T>>(listenerToRemove, listeners);
    }

    /// <summary>
    /// Removes all listeners observing this observable
    /// </summary>
    public static void Remove(this IObservable observable)
    {
        observableEventListeners.Remove(observable);
    }

    #endregion

    private static void AddListener<T>(EventBusEvent<T> eventToAdd) where T : BaseEvent
    {
        AddListener(eventToAdd, eventListeners);
    }

    private static void AddListener<T>(EventBusEvent<T> eventToAdd,
                                    Dictionary<Type, List<EventBusEvent>> listeners) where T : BaseEvent
    {
        if(!initialized)
        {
            Initialize();
        }

        if(!listeners.ContainsKey(typeof(T)))
        {
            listeners.Add(typeof(T), new List<EventBusEvent>());
        }
        listeners[typeof(T)].Add(eventToAdd);
    }

    private static void RemoveListener<EventT, EventBusEventT>(Action<object, EventT> listenerToRemove,
                                                            Dictionary<Type, List<EventBusEvent>> listeners)
		where EventT : BaseEvent
		where EventBusEventT : EventBusEvent<EventT>
    {
        if(listeners.ContainsKey(typeof(EventT)))
        {
            EventBusEvent eventToRemove = listeners[typeof(EventT)]
				.OfType<EventBusEventT>()
				.FirstOrDefault(ev => ev.EventEquals(listenerToRemove));

            if(eventToRemove != null)
            {
                listeners[typeof(EventT)].Remove(eventToRemove);
            }
        }
    }

    private static void Initialize()
    {
        SceneManager.activeSceneChanged += SceneChanged;
        SceneManager.sceneUnloaded += SceneUnloaded;

        initialized = true;
    }

    private static void SceneChanged(Scene oldScene, Scene newScene)
    {
        TriggerEvent(null, new GameStateEvent.SceneLoadedEvent(newScene.name, newScene.buildIndex));
    }

    private static void SceneUnloaded(Scene oldScene)
    {
		if (oldScene.name != "Preview Scene")
		{
			Debug.Log("Unloading " + oldScene.name + ". If you just started the game, this shouldn't happen");
			eventListeners.Clear(); 
		}
    }
}