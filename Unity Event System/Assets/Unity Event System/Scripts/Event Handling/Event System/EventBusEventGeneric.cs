﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EventBusEvent<T> : EventBusEvent where T : BaseEvent
{
	protected Action<object, T> action;

    protected Predicate<T> predicate;

    protected EventBusEvent(Action<object, T> action, Predicate<T> predicate)
	{
		this.action = action;
        if (predicate == null)
        {
            this.predicate = (e) => true;
        }
        else
        {
            this.predicate = predicate;
        }
	}

    public override bool IsTarget(object target)
    {
        return target.Equals(action.Target);
    }

    public override void Trigger(object sender, BaseEvent e)
	{
		Trigger(sender, (T)e);
	}

	public bool EventEquals(Action<object, T> other)
	{
		return action.Equals(other);
	}

	protected virtual void Trigger(object sender, T e)
	{
		if (action != null && predicate.Invoke(e))
		{
			action.Invoke(sender, e);
		}
	}
}
