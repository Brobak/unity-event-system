# Unity Event System

This is an event system for use in Unity that allows an object to subscibe to en eventype and expect some parameters back. Any script can then fire an event and give it the needed parameters and if any are subscribed to it, they will be triggered.

# How to use

To use the event system in your project simply download the Unity package "Event System" from the root and import it in your project. To see how to subscribe to and fire events see the examples.

# License
/* 
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <nicolai0brobak@gmail.com> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.   Nicolai Brobak
 * ----------------------------------------------------------------------------
 */